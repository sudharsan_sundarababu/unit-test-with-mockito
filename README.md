# Unit testing

In the testing pyramid, unit testing is the more granular test and are the quick wins.

For every programming language/framework, there exists a good unit testing framework.

For Java-based project, Mockito seems to be the de facto unit testing framework.

This repository is an attempt to try hands-on with Mockito.

## Run the tests

To run the test, execute these maven commands.
```
> mvn clean test
```
