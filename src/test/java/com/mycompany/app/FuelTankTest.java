package com.mycompany.app;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;


public class FuelTankTest {
    private FuelTank fuelTank;

    @BeforeEach
    public void setup() {
        fuelTank = new FuelTank();
    };

    @Test
    public void getFuelReadingTest() {
        assertDoesNotThrow(() -> {
           fuelTank.setFuel(100);
           assertEquals(100, fuelTank.getFuel());
        });
    }

    @Test
    public void setFuelReadingTest() {
        assertDoesNotThrow(() -> {
            fuelTank.setFuel(108);
        });
        assertEquals(108, fuelTank.getFuel());
    }

    }
