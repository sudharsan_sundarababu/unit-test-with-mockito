package com.mycompany.app;

import org.mockito.Mockito;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.when;

public class CarTest {
    private Car car;
    private Engine engine;
    private FuelTank fuelTank;

    @BeforeEach
    public void setup() {
        engine = Mockito.mock(Engine.class);
        fuelTank = Mockito.mock(FuelTank.class);
        car = new Car(engine, fuelTank);
    }

    @Test
    public void startWithFuel() {
        assertDoesNotThrow(() -> {
            when(engine.isRunning()).thenReturn(false).thenReturn(true);
            when(fuelTank.getFuel()).thenReturn(100);
            car.start();
        });
    }

    @Test
    public void startWithNoFuel() {
        assertThrows(IllegalStateException.class,
                () -> {
                    when(engine.isRunning()).thenReturn(false);
                    when(fuelTank.getFuel()).thenReturn(0);
                    car.start();
                },
                "Can't start: no fuel");
    }

    @Test
    public void startAlreadyStartedEngine() {
        assertThrows(IllegalStateException.class,
                () -> {
                    when(engine.isRunning()).thenReturn(true);
                    when(fuelTank.getFuel()).thenReturn(0);
                    car.start();
                },
                "Engine already running");
    }

    @Test
    public void engineStartFailed() {
        assertThrows(IllegalStateException.class,
                () -> {
                    when(engine.isRunning()).thenReturn(true).thenReturn(false);
                    when(fuelTank.getFuel()).thenReturn(0);
                    car.start();
                },
                "Started engine but isn't running");
    }

}
