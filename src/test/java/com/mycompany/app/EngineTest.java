package com.mycompany.app;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class EngineTest {
    private Engine engine;

    @BeforeEach()
    public void setup() {
        engine = new Engine();
    }

    @Test
    public void engineStartTest() {
        assertDoesNotThrow(() -> {
            engine.start();
            assertEquals(true, engine.isRunning());
        });
    }

    @Test
    public void engineStopTest() {
        assertDoesNotThrow(() -> {
            engine.stop();
            assertEquals(false, engine.isRunning());
        });
    }
}
